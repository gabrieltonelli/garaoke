import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import {
  Card,
  CardContent,
  Grid,
  Typography,
  Tabs,
  Tab,
  Box,
  Button,
  TextField,
  FormGroup,
  FormControlLabel,
  Checkbox,
} from '@mui/material';
import CssBaseline from '@mui/material/CssBaseline';
import PropTypes from 'prop-types';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import HeaderAppBar from '../components/HeaderAppBar';
import validations from './helpers/validations'
function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

function Config({ 
                  auth, 
                  user, 
                  userSettings, 
                  saveUserSettings, 
                  setAllowInteractivity,
                  allowInteractivity,
                  setShowControlsPlayer,
                  showControlsPlayer,
                  setVideoId,
                  videoId
                }) {
  const [value, setValue] = useState(0);
  

  const [apiKeys, setApiKeys] = useState(userSettings.configs.apiKeys);
  const [intros, setIntros] = useState(userSettings.configs.intros);
  const [transitions, setTransitions] = useState(userSettings.configs.transitions);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const navigate = useNavigate();
  const handleApiKeyChange = (index, value) => {
    const newApiKeys = [...apiKeys];
    newApiKeys[index] = value;
    setApiKeys(newApiKeys);
  };

  const handleIntroChange = (index, value) => {
    const newIntros = [...intros];
    newIntros[index] = value;
    setIntros(newIntros);
  };

  const handleTransitionChange = (index, value) => {
    const newTransitions = [...transitions];
    newTransitions[index] = value;
    setTransitions(newTransitions);
  };

  const cancel = () => {
    navigate('/');
  };

  const buttonStyles = {
    marginRight: '10px',
  };

  const handleSaveUserSettings=(e)=>{
    e.preventDefault();
    auth.onAuthStateChanged(user=>{
      let newUserSettings = userSettings;
      switch(e.target.pannelIndex.value){
        case 'general':
          newUserSettings.configs.general = {
                                              showControlsPlayer: e.target.showControlsPlayer.checked,
                                              allowInteractivity: e.target.allowInteractivity.checked,
                                              theme: 'dark',
                                            };
          break;
        case 'apiKeys':
          newUserSettings.configs.apiKeys = apiKeys;
          break;
        case 'intros':
          newUserSettings.configs.intros = intros;
          break;
        case 'transitions':
          newUserSettings.configs.transitions = transitions;
          break;
        default:
      }
      saveUserSettings(user.uid, newUserSettings);
    })
  }

  const apiKeysErrors = validations.checkApiKeysErrors(apiKeys)
  const introsErrors = validations.checkIntrosErrors(intros)
  const transitionsErrors = validations.checkTransitionsErrors(transitions)

  return (
    <div style={{ height: '100vh', display: 'flex', flexDirection: 'column', background: '#000000' }}>
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <div style={{ height: '100vh', display: 'flex', flexDirection: 'column' }}>
        <HeaderAppBar 
          user={user} 
          setVideoId={setVideoId}
          videoId={videoId}
        />
        <main style={{ flexGrow: 1, overflow: 'auto' }}>
          <Grid container spacing={0}>
            <Grid item xs={12} md={12}>
              <Card elevation={3} style={{ padding: '16px', margin: '10px', height: 'calc(100vh - 100px)' }}>
                <CardContent>
                  <Grid container direction="row" justifyContent="space-evenly">
                    <Grid item xs={12} md={12}>
                      <Typography variant="h6" component="div">
                        Configuraciones
                      </Typography>
                    </Grid>
                    <Grid item xs={12} md={12}>
                    <form autoComplete='off' className='form-group'  onSubmit={handleSaveUserSettings}>
                      <Box sx={{ width: '100%' }}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                          <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" centered>
                            <Tab label="Generales" {...a11yProps(0)} style={{ textTransform: 'none' }} />
                            <Tab label="API Keys de Youtube" {...a11yProps(1)} style={{ textTransform: 'none' }} />
                            <Tab label="Intros" {...a11yProps(2)} style={{ textTransform: 'none' }} />
                            <Tab label="Transiciones" {...a11yProps(3)} style={{ textTransform: 'none' }} />
                          </Tabs>
                        </Box>
                        <CustomTabPanel value={value} index={0}><input type='hidden' id='pannelIndex' value='general' />
                          <div id="configPannel1" class="configpannel_container scroll">
                            <FormGroup>
                              <FormControlLabel control={<Checkbox inputProps={{ "aria-label": "controlled" }} checked={showControlsPlayer} onClick={()=>setShowControlsPlayer(!showControlsPlayer)} id='showControlsPlayer'/>} label="Mostrar controles de reproducción en pantalla principal." />
                              <FormControlLabel control={<Checkbox inputProps={{ "aria-label": "controlled" }} checked={allowInteractivity} onClick={()=>setAllowInteractivity(!allowInteractivity)} id='allowInteractivity'/>} label="Permitir monitor de interactividad para que el público presente pueda vincularse desde sus teéfonos mediante QR para poder seguir canciones, calificar performances, etc." />
                            </FormGroup>
                          </div>
                        </CustomTabPanel>
                        <CustomTabPanel value={value} index={1}><input type='hidden' id='pannelIndex' value='apiKeys' />
                          <div id="configPannel2" class="configpannel_container scroll">
                            Establece una o más API key de Youtube para habilitar a que Garaoke realice búsquedas de videos en la plataforma. Debido a que las API Key ofrecidas por Youtube cuentan con un límite de resultados diarios, te aconsejamos habilitar más de una, de modo que no sufras cortes por esta restricción al momento de tu show 😜.
                            <p>Puedes generar API key's desde el panel de desarrollador de Youtube siguiendo estos pasos: 
                            <ol>
                              <li>Dirígete a <a rel="noreferrer" href='https://console.cloud.google.com/apis/credentials' target='_blank'>https://console.cloud.google.com/apis/credentials</a></li>
                              <li>Botón [Crear credenciales] luego selecciona [Clave de API].</li>
                              <li>Listo! copia la clave desde la ventana de notificación del sitio y pégala aquí abajo. No olvides presionar en [Guardar] de este panel más abajo.</li>
                            </ol>
                            </p>
                            {apiKeys.map((key, index) => (
                              <TextField
                                key={`apikey_${index}`}
                                label={`API Key ${index + 1}`}
                                variant="outlined"
                                fullWidth
                                value={key}
                                onChange={(e) => handleApiKeyChange(index, e.target.value)}
                                style={{ marginBottom: '10px' }}
                                error={apiKeysErrors['apikey_'+index].error}
                                helperText={apiKeysErrors['apikey_'+index].errorMsg}
                              />
                            ))}
                          </div>
                        </CustomTabPanel>
                        <CustomTabPanel value={value} index={2}><input type='hidden' id='pannelIndex' value='intros' />
                          <div id="configPannel3" class="configpannel_container scroll">
                            Aquí puedes establecer el conjunto de videos que usarás para lanzar una intro de presentación de tu fiesta de karaoke. 
                            <p/>
                            {intros.map((key, index) => (
                              <TextField
                                key={`intro_${index}`}
                                label={`Intro ${index + 1}`}
                                variant="outlined"
                                fullWidth
                                value={key}
                                onChange={(e) => handleIntroChange(index, e.target.value)}
                                style={{ marginBottom: '10px' }}
                                error={introsErrors['intro_'+index].error}
                                helperText={introsErrors['intro_'+index].errorMsg}
                              />
                            ))}
                          </div>
                        </CustomTabPanel>
                        <CustomTabPanel value={value} index={3}><input type='hidden' id='pannelIndex' value='transitions' />
                        <div id="configPannel4" class="configpannel_container scroll">
                            Aquí puedes establecer algunos videos de transiciones. Luego podrás elegir cual de ellos es el que se activará al momento de finalizar la canción de karaoke (o video) que se esté reproduciendo. De esta forma siempre habrá algo reproduciéndose en pantalla!. 
                            <p/>
                            {transitions.map((key, index) => (
                              <TextField
                                key={`transition_${index}`}
                                label={`Transición ${index + 1}`}
                                variant="outlined"
                                fullWidth
                                value={key}
                                onChange={(e) => handleTransitionChange(index, e.target.value)}
                                style={{ marginBottom: '10px' }}
                                error={transitionsErrors['transition_'+index].error}
                                helperText={transitionsErrors['transition_'+index].errorMsg}
                              />
                            ))}
                          </div>
                        </CustomTabPanel>
                      </Box>
                      <div>
                        <Button type="submit" variant="contained" style={buttonStyles}>
                          Guardar
                        </Button>
                        <Button variant="contained" onClick={cancel} style={buttonStyles}>
                          Volver al panel
                        </Button>
                      </div>
                      </form>
                    </Grid>
                  </Grid>
                </CardContent>
              </Card>
            </Grid>
          </Grid>
        </main>
      </div>
    </ThemeProvider>
    </div>
  );
}

export default Config;
