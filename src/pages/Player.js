
import React, { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import 'mui-player/dist/mui-player.min.css'
import YouTube from 'react-youtube';
import PlayerModal from '../components/PlayerModal';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

function Player({updateUserField, currentPlaylistItem, 
  currentTransitionVideo, currentIntroVideo, allowInteractivity, 
  showControlsPlayer, setVideoId, videoId
  }) {

  const [openModal, setOpenModal] = useState(false);
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const [modalContentType, setModalContentType] = useState('');
  const [message, setMessage] = useState(null);
  const [intervalId, setIntervalId] = useState(null);
  const [queryQR, setQueryQR] = useState('');
  const [height, setHeight] = useState(window.innerHeight - 10);
  const [currentIntroVideo2, setCurrentIntroVideo2] = useState(currentIntroVideo);
  const [currentTransitionVideo2, setCurrentTransitionVideo2] = useState(currentTransitionVideo);
  const [currentPlaylistItem2, setCurrentPlaylistItem2] = useState(currentPlaylistItem);

  const opts = {
    height: height,
    width: '100%',
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      controls: showControlsPlayer ? 1 : 0,
      autoplay: 1,
      modestbranding: 1,
      rel: 0,
      showinfo: 0,
      iv_load_policy: 3
    },
  };

  var tar = null

  window.addEventListener('resize', function() {
    setHeight(window.innerHeight - 10);
  });

  const handleCloseModal = () => {
    setOpenModal(false);
  };

  const onEndVideo = () => {
    if(currentTransitionVideo2 === videoId){
      //fix when transition end
      setVideoId(null) 
      setTimeout(()=>{
        setVideoId(currentTransitionVideo2)
      }, 1000)
    }else{
      setVideoId(currentTransitionVideo2)
    }
    updateUserField(
      'currentVideoData',
      JSON.stringify(
        {
          videoId: null, 
          time: 0, 
          assignedTo: null
        }
      )
    )
  };

  const onPlayVideo = (e) => {
    tar = e.target
    const video_id = tar.playerInfo.videoData.video_id
    if((video_id === currentTransitionVideo2 || video_id === currentIntroVideo2 )){
      if(intervalId){
        clearInterval(intervalId)
      } 
    }else{
      if(video_id !== videoId){ //when change video from the player, ex: select from suggested videos
        setVideoId(video_id)
      }
    }
  };

  const getCurrentTime = () => {
    if(tar){
        updateUserField('currentVideoTime', tar.getCurrentTime())
        console.debug('Sincronizando video ', tar.playerInfo.videoData.video_id, ' con time=', tar.getCurrentTime())
    } 
  }

  const setTheInterval = () => {
    const newIntervalId = setInterval(getCurrentTime, 1000)
    setIntervalId(newIntervalId)
  }

  const onReadyVideo = (event) => {
    if(intervalId){
      clearInterval(intervalId)
    }
    if(event.target){
      tar = event.target
      if(tar.playerInfo.videoData.video_id !== currentTransitionVideo2 && tar.playerInfo.videoData.video_id !== currentIntroVideo2 ){
        if(allowInteractivity){
          console.log('Begin sincronization of video '+tar.playerInfo.videoData.video_id)
          console.log('currentPlaylistItem2 ',currentPlaylistItem2)
          const currentVideoData = {
            videoId: tar.playerInfo.videoData.video_id,
            assignedTo: currentPlaylistItem2 && currentPlaylistItem2.playlist ? currentPlaylistItem2.item.assignedTo : '',
            votes: currentPlaylistItem2 && currentPlaylistItem2.playlist ? currentPlaylistItem2.item.votes : [],
            playlist: currentPlaylistItem2 && currentPlaylistItem2.playlist ? currentPlaylistItem2.playlist : ''
          }
          setTimeout(() =>
            updateUserField(
              'currentVideoData',
              JSON.stringify(currentVideoData)
            )
          ,2000)
          setTimeout(setTheInterval,2000) 
        }else{
          console.log('Sincronization is off')
        }
      }else{
          console.log('Begin play transition or intro')
          tar = null
          const currentVideoData = {
            videoId: null,
            assignedTo: '',
            votes: [],
            playlist: ''
          }
          setTimeout(() =>
            updateUserField(
              'currentVideoData',
              JSON.stringify(currentVideoData)
            )
          ,2000)
      }
    }
  }

  /* eslint-disable */

  useEffect(() => {
    const handleMessage = (event) => {
      const data = event.data;
      if(data.message)
        setMessage(data.message)
      switch (data.type) {
        case 'changeVideo':
          setVideoId(data.videoId);
          break;
        case 'changeIntro':
          setCurrentIntroVideo2(data.introId);
          break;  
        case 'changeTransition':
          setCurrentTransitionVideo2(data.transitionId);
          break;   
        case 'changePlaylistItem':
          setCurrentPlaylistItem2(data.playlistItem);
          break;    
        case 'showHideModalQR':
          setModalContentType('QR')
          setOpenModal(!data.modalQR)
          setQueryQR(data.query)
          break;  
        case 'showHideModalMessage':
          setModalContentType('Message')
          setOpenModal(!data.modalMessage)
          break; 
        case 'showHideModalVotesTable':
          setModalContentType('Votes')
          setOpenModal(!data.modalVotes)
          break;   
        default:;  
      }
    };
    window.addEventListener('message', handleMessage);
    return () => {
      window.removeEventListener('message', handleMessage);
    };
  }, []); 

  useEffect(() => {
    setVideoId(queryParams.get('videoId') ?? currentTransitionVideo)
  }, []); 

  /* eslint-enable */

  return (
    <ThemeProvider theme={darkTheme}>
      <div style={{ height: '100vh', display: 'flex', flexDirection: 'column', background: '#000000' }}>
        <PlayerModal 
          open={openModal} 
          onClose={handleCloseModal} 
          type={modalContentType} 
          message={message} 
          queryQR={queryQR}
          userId={queryParams.get('userId') }
        />
        <CssBaseline />
        {videoId &&
        <YouTube 
          videoId={videoId} 
          onPlay={onPlayVideo}
          opts={opts} 
          onEnd={onEndVideo} 
          onEndVideo={onEndVideo} 
          onReady={onReadyVideo}
        />}
      </div>
    </ThemeProvider>
  );
}

export default Player;
