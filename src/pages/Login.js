import React from 'react';
import {
  Dialog, DialogTitle, DialogContent,
  Typography,
  Button,
  Box,
} from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { styled } from '@mui/material/styles';
import { auth, provider } from '../components/FirebaseConfig';
import { signInWithPopup } from 'firebase/auth';


const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

function Login({setUser}) {
  const handleGoogleSignIn=()=>{
    signInWithPopup(auth, provider).then((result)=>{
      const user = result.user;
      console.log(user);
      setUser(user);
    }).catch((err)=>{
      console.log(err);
    })
  }

  const GoogleIcon = () => {
    return <img alt="Google Icon" src={googlePngIcon} width="24" height="24"/>
  };

  const googlePngIcon = '../../../assets/images/google-icon32.png';

  return (
    <ThemeProvider theme={darkTheme}>
      <BootstrapDialog
        aria-labelledby="customized-dialog-title"
        open={true}
      >
        <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          Ingresar a Garaoke
        </DialogTitle>
       
        <DialogContent dividers>
          <Typography gutterBottom>
            <p>Bienvenido a Garaoke 👋🏻, el karaoke 🎤 más simple, portable, interactivo y totalmente 🆓GRATIS que existe en el mundo!</p>
            <p>Para brindarte una experiencia personalizada😊 necesitamos que ingreses con tu cuenta de Google en simples clicks.</p>
            <p>Como usuario registrado podrás guardar tus listas de reproducción y acceder a las funciones de interactividad para 
              que todos los que participen de tu fiesta de karaoke, desde sus smartphones 📱 puedan:</p>
            <ul>
              <li>✅ Seguir la letra de las canciones</li>
              <li>✅ Calificar a los participantes</li>
              <li>✅ Pedir canciones</li>
              <li>Y más...!!</li>
            </ul>
            <Box sx={{padding: 2, textAlign: 'center'}}>
              <Button variant="contained"
                size="large"
                startIcon={<GoogleIcon />}
                onClick={handleGoogleSignIn}>
                Ingresar con Google
              </Button> 
            </Box>
          </Typography>
        </DialogContent>
      </BootstrapDialog>
      <CssBaseline />
    </ThemeProvider>
  );
}

export default Login;
