import React, {useState} from 'react';

import HeaderAppBar from '../components/HeaderAppBar';
//pannels
import SearchCard from '../components/SearchCard';
import PlayListCard from '../components/PlayListCard';
import PreviewCard from '../components/PreviewCard';
import EffectsCard from '../components/EffectsCard';
import SearchResult from '../components/SearchResult';
//
import { Grid } from '@mui/material';

function Pannel({
                  playlist, 
                  setKeySearch, 
                  setVideosSearchResponse, 
                  keySearch, 
                  videosSearchResponse, 
                  setPlaylist, 
                  setPreviewVideoId, 
                  previewVideId, 
                  onIsKaraokeOnly, 
                  isKaraokeOnly,
                  setCurrentIntroVideo,
                  setCurrentTransitionVideo,
                  currentIntroVideo,
                  currentTransitionVideo,
                  user,
                  userSettings, 
                  openVideoPlayer,
                  showHideModalQR,
                  showHideModalMessage,
                  showModalMessage,
                  videoPlayer,
                  setSnackMessage,
                  selectedApiKey,
                  setSelectedApiKey,
                  currentPlaylist,
                  setCurrentPlaylist,
                  userPresetPlaylists,
                  originPresetPlaylists,
                  setOriginPresetPlaylists,
                  originPresetPlaylist,
                  setOriginPresetPlaylist,
                  userPresetPlaylist,
                  setUserPresetPlaylist,
                  handleGoogleSignOut,
                  setCurrentPlaylistItem,
                  allowInteractivity,
                  currentPlaylistItem,
                  setVideoId,
                  videoId,
                  showHideModalVotesTable
                 }) {

    const [reloadPlaylist , setReloadPlaylist] = useState()

    return (
      <div style={{ height: '100vh', display: 'flex', flexDirection: 'column', background: '#000000' }}>
        <HeaderAppBar 
          user={user} 
          openVideoPlayer={openVideoPlayer} 
          allowInteractivity={allowInteractivity}
          setCurrentIntroVideo={setCurrentIntroVideo} 
          setCurrentTransitionVideo={setCurrentTransitionVideo} 
          currentIntroVideo={currentIntroVideo} 
          currentTransitionVideo={currentTransitionVideo} 
          showHideModalQR={showHideModalQR}
          showHideModalMessage={showHideModalMessage}
          videoPlayer={videoPlayer}
          userSettings={userSettings}
          handleGoogleSignOut={handleGoogleSignOut}
          setVideoId={setVideoId}
          showModalMessage={showModalMessage}
          showHideModalVotesTable={showHideModalVotesTable}
        />

        <main style={{ flexGrow: 1, overflow: 'auto' }}>
          <Grid container spacing={0}>

            <Grid item xs={12} md={4}>
              <SearchCard 
                setKeySearch={setKeySearch} 
                keySearch={keySearch} 
                setVideosSearchResponse={setVideosSearchResponse} 
                isKaraokeOnly={isKaraokeOnly} 
                onIsKaraokeOnly={onIsKaraokeOnly} 
                setSnackMessage={setSnackMessage}
                userSettings={userSettings}
                selectedApiKey={selectedApiKey}
                setSelectedApiKey={setSelectedApiKey}
                
              />
              <PlayListCard 
                playlist={playlist} 
                openVideoPlayer={openVideoPlayer} 
                videoPlayNow={openVideoPlayer} 
                setReloadPlaylist={setReloadPlaylist} 
                reloadPlaylist={reloadPlaylist}
                setPlaylist={setPlaylist} 
                currentPlaylist={currentPlaylist}
                setCurrentPlaylist={setCurrentPlaylist}
                userPresetPlaylists={userPresetPlaylists}
                originPresetPlaylists={originPresetPlaylists}
                setOriginPresetPlaylists={setOriginPresetPlaylists}
                originPresetPlaylist={originPresetPlaylist}
                setOriginPresetPlaylist={setOriginPresetPlaylist}
                userPresetPlaylist={userPresetPlaylist}
                setUserPresetPlaylist={setUserPresetPlaylist}
                setCurrentPlaylistItem={setCurrentPlaylistItem}
                currentPlaylistItem={currentPlaylistItem}
                setVideoId={setVideoId}
                videoId={videoId}

              />
            </Grid>

            <Grid item xs={12} md={4}>
              <SearchResult 
                videos={videosSearchResponse} 
                setPreviewVideoId={setPreviewVideoId} 
                openVideoPlayer={openVideoPlayer} 
                setPlaylist={setPlaylist} 
                playlist={playlist} 
                setReloadPlaylist={setReloadPlaylist} 
                currentPlaylist={currentPlaylist}
                setCurrentPlaylistItem={setCurrentPlaylistItem}
                currentPlaylistItem={currentPlaylistItem}
                setVideoId={setVideoId}
                videoId={videoId}

              />
            </Grid>

            <Grid item xs={12} md={4}>
              <PreviewCard videoId={previewVideId}/>
              <EffectsCard/>
            </Grid>

          </Grid>
        </main>
      </div>
    
  );
}
export default Pannel;