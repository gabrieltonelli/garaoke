import React from 'react';
import {
  Dialog, DialogTitle, DialogContent,
  Typography,
} from '@mui/material';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { styled } from '@mui/material/styles';
import { Navigate } from 'react-router-dom';


const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

function NotFound() {
  return (
    <ThemeProvider theme={darkTheme}>
      <BootstrapDialog
        aria-labelledby="customized-dialog-title"
        open={true}
      >
        <DialogTitle align='center' sx={{ m: 0, p: 2 }} id="customized-dialog-title">
          404
        </DialogTitle>
        <DialogContent dividers>
          <Typography gutterBottom align='center'>
            <p>La página solicitada no existe o ya no tiene sentido que estés aquí.</p>
            <p>Redirigiendo al inicio...</p>
          </Typography>
        </DialogContent>
      </BootstrapDialog>
      <CssBaseline />
      <Navigate to="/" />
    </ThemeProvider>
    
  );
}

export default NotFound;
