const noError = { 
    error: false,
    errorMsg: '', 
};

const apiKeyError = {
  error: true,
  errorMsg: 'La Api Key ingresada no es válida.',
};
const videoCodeError = {
  error: true,
  errorMsg: 'El código del video de Youtube ingresado no es válido.',
};

const checkYoutubeVideoCode = (videoCode) => {
    if (!videoCode) return noError;
    if(!(/^[a-zA-Z0-9_-]{11}$/.test(videoCode))) return videoCodeError;
    return noError;

};

const checkYoutubeApiKey = (apiKey) => {
    if (!apiKey) return noError;
    if(!(/^[a-zA-Z0-9_-]{39}$/.test(apiKey))) return apiKeyError;
    return noError;
};

const checkApiKeysErrors = (apikeys) => {
  let errors = [];
  apikeys.forEach((value, index) => {
    errors['apikey_'+index] = checkYoutubeApiKey(value)
  });  
  return errors;
};

const checkIntrosErrors = (intros) => {
    let errors = [];
    intros.forEach((value, index) => {
      errors['intro_'+index] = checkYoutubeVideoCode(value)
    });  
    return errors;
};

const checkTransitionsErrors = (transitions) => {
    let errors = [];
    transitions.forEach((value, index) => {
      errors['transition_'+index] = checkYoutubeVideoCode(value)
    });  
    return errors;
};

const validations = {
  checkApiKeysErrors,
  checkIntrosErrors,
  checkTransitionsErrors
}
export default validations;
