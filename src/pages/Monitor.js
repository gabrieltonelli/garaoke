
import React, { useEffect, useState } from 'react';
import { db, doc, onSnapshot } from '../components/FirebaseConfig';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import 'mui-player/dist/mui-player.min.css'
import YouTube from 'react-youtube';
import { useLocation } from 'react-router-dom';
import { Box, CircularProgress } from '@mui/material';
import VotationModal from '../components/VotationModal';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

function Monitor({user, updateUserField}) {
  const [videoId, setVideoId] = useState('');
  const [height, setHeight] = useState(window.innerHeight - 10);
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const userId = queryParams.get('UID');
  const [currentVideoData, setCurrentVideoData] = useState(null);
  const [currentVideoDataAux, setCurrentVideoDataAux] = useState(null);
  const [currentVideoTime, setCurrentVideoTime] = useState(0);
  const [tar, setTar] = useState(null);
  const [totalVideoTime, setTotalVideoTime] = useState(null);
  const [showVotation, setShowVotation] = useState(null);
  const [stars, setStars] = useState(0);
  const [playlist, setPlaylist] = useState([]);
  const [presetPlaylists, setPresetPlaylists] = useState([]);
  
  /* eslint-disable */
  useEffect(() => {
    const docRef = doc(db, 'users', userId);
    const unsubscribe = onSnapshot(docRef, (doc) => {
      if (doc.exists()) {
        const videoData = doc.data().currentVideoData
        if(videoData){
          const parsedVideoData = JSON.parse(videoData)
          if(parsedVideoData.videoId){
            console.log('Sincronized: ',parsedVideoData);
            setCurrentVideoData(parsedVideoData);
            setCurrentVideoTime( parseInt(doc.data().currentVideoTime, 10) );
          }else{
            setCurrentVideoData(null);
            setCurrentVideoTime( 0 );
            setTotalVideoTime(null)
          }
        }

       // const playlistData = doc.data().playlist
        if(doc.data().playlist){
          setPlaylist(doc.data().playlist)
        }else{
          setPlaylist([]);
        }

        //const presetPlaylistsData = doc.data().presetPlaylists
        if(doc.data().presetPlaylists){
          setPresetPlaylists(doc.data().presetPlaylists)
        }else{
          setPresetPlaylists([]);
        }

      } else {
        console.log('Document does not exist');
      }
    });
    return () => {
      unsubscribe();
    };
  }, []);

  const opts = {
    height: height,
    width: '100%',
    playerVars: {
      // https://developers.google.com/youtube/player_parameters
      controls: 0,
      autoplay: 1,
      modestbranding: 1,
      rel: 0,
      showinfo: 0,
      iv_load_policy: 3,
      fs: 0,
      disablekb: 1,
      mute: true,
    },
  };

  useEffect(() => {
    if(currentVideoData && currentVideoData.videoId){
      if(currentVideoData.videoId !== videoId){
        setVideoId(currentVideoData.videoId)
        setCurrentVideoDataAux(currentVideoData)
      }
    }else{
      setVideoId(null)
    }
  }, [currentVideoData]);

  useEffect(() => {
    if(currentVideoData && currentVideoData.videoId && currentVideoTime){
      if(tar && videoId && (currentVideoTime + 1) < tar.playerInfo.duration){
        console.debug('Seek to :', currentVideoTime + 1);
        try{
          tar.seekTo(currentVideoTime + 1)
        }catch(error){
          console.error(error)
        }
      }
    }
  }, [currentVideoTime, currentVideoData, tar, videoId]);

  useEffect(() => {
    if(currentVideoData && currentVideoData.videoId && currentVideoData.assignedTo ){
      if(totalVideoTime && currentVideoTime > (totalVideoTime - 13)){
        setShowVotation(true)
      }
    }
  }, [currentVideoTime, currentVideoData]);

  /* eslint-enable */
  
  window.addEventListener('resize', function() {
    setHeight(window.innerHeight - 10);
  });

  const onVideoReady = (event) => {
    console.debug(event.target)
    if(event.target){
      setTar(event.target)
      setTotalVideoTime(event.target.playerInfo.duration)
    }
  }

  const onVideoEnd = (event) => {
      setVideoId('')
      setTar(null)
  }

  const addOrUpdateVote = (item, user, votation) => {
    const existingVoteIndex = item.votes.findIndex(vote => vote.userId === user.uid);
    if (existingVoteIndex !== -1) {
      item.votes[existingVoteIndex].userName = user.displayName;
      item.votes[existingVoteIndex].vote = votation;
    } else {
      item.votes.push({
        userId: user.uid,
        userName: user.displayName,
        vote: votation
      });
    }
    return item
  }

  const saveVotation = (votation) => {
    if(!votation) return;
    let plist = [];
    if(currentVideoDataAux.playlist === 'user_playlist'){
      plist = playlist;
    }else{
      const pplist = presetPlaylists.find( pp => pp.name === currentVideoDataAux.playlist)
      if(pplist.items)
      plist = pplist.items;
    }
    let item = plist.find((it) => it.videoId === currentVideoDataAux.videoId);
    const newItem = addOrUpdateVote(item, user, votation)
    if(currentVideoDataAux.playlist === 'user_playlist'){
      const plistIndex = playlist.findIndex(it => it.videoId === currentVideoDataAux.videoId);
      if (plistIndex !== -1) {
        playlist[plistIndex] = newItem;
        updateUserField('playlist', playlist, userId);
      }
    }else{
      const presetPlistIndex = presetPlaylists.findIndex(pp => pp.name === currentVideoDataAux.playlist);
      if (presetPlistIndex !== -1) {
        const pplistIndex = presetPlaylists[presetPlistIndex].items.findIndex(it => it.videoId === currentVideoDataAux.videoId);
        if (pplistIndex !== -1) {
          presetPlaylists[presetPlistIndex].items[pplistIndex] = newItem;
          updateUserField('presetPlaylists', presetPlaylists, userId);
        }
      }
    }
  }

  /* eslint-disable */

  useEffect(() => {
    if(showVotation === false){
      saveVotation(stars)
      setStars(0)
    }
  }, [showVotation]);

  /* eslint-enable */

  return (
            <>
              {showVotation && currentVideoData && currentVideoData.videoId && 
              <VotationModal 
                targetVideo={tar} 
                currentVideoData={currentVideoData} 
                setShowVotation={setShowVotation} 
                updateUserField={updateUserField} 
                userId={userId}
                stars={stars} 
                setStars={setStars}
              />}
              <ThemeProvider theme={darkTheme}>
                <div style={{ height: '100vh', display: 'flex', flexDirection: 'column', background: '#000000' }}>
                  <CssBaseline />
                  {videoId &&
                  <YouTube 
                    videoId={videoId} 
                    opts={opts} 
                    onReady={onVideoReady}
                    onEnd={onVideoEnd}
                  />
                  }
                  {!videoId &&
                  <>
                    <Box display="flex" justifyContent="center" alignItems="center" minHeight={'50vh'} >
                      <p style={{textAlign: 'center'}}>
                        Aguarda por favor.
                        Esperando el próximo video...
                      </p>
                    </Box>
                    <Box display="flex" justifyContent="center" alignItems="center" >
                      <CircularProgress />
                    </Box>
                  </>
                  }
                </div>
              </ThemeProvider>
            </>
          );
}

export default Monitor;
