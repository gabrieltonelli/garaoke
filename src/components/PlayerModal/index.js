import React, { useEffect, useState } from 'react';
import CloseIcon from '@mui/icons-material/Close';
import {
    IconButton, Dialog, DialogTitle, DialogContent,
    Typography,
    TableContainer,
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
  } from '@mui/material';
  import { tableCellClasses } from '@mui/material/TableCell';
import { styled } from '@mui/material/styles';
import QRCode from "react-qr-code";
import Paper from '@mui/material/Paper';
import Draggable from 'react-draggable';
import StarIcon from '@mui/icons-material/Star';
import StarHalfIcon from '@mui/icons-material/StarHalf';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import { db, doc, onSnapshot } from '../FirebaseConfig';


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));


function PaperComponent(props) {
    return (
        <Draggable
        handle="#draggable-dialog-title"
        cancel={'[class*="MuiDialogContent-root"]'}
        >
        <Paper {...props} />
        </Draggable>
    );
}
///////////////////////////////////////////////////////////////////////////////////////////////
function createData(player, votes, stars) {
    return { player, votes, stars };
}
  

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
        fontSize: 14,
    },
}));

   

export default function PlayerModal({
    open, onClose, type, message, queryQR,
    userId,
    }) {
    const [rows, setRows] = useState(null);    
    const [playlist, setPlaylist] = useState(null);
    const [presetPlaylists, setPresetPlaylists] = useState(null);    

    /* eslint-disable */
    useEffect(() => {

        if(userId === -1) return;
        const docRef = doc(db, 'users', userId);

        const unsubscribe = onSnapshot(docRef, (doc) => {
        if (doc.exists()) {
            if(doc.data().playlist){
                setPlaylist(doc.data().playlist)
              }else{
                setPlaylist([]);
              }
              if(doc.data().presetPlaylists){
                setPresetPlaylists(doc.data().presetPlaylists)
              }else{
                setPresetPlaylists([]);
              }

            } else {
              console.log('Document does not exist');
            }
        });
        return () => {
        unsubscribe();
        };
    }, [userId]);


    useEffect(() => {
        if(playlist !== null && presetPlaylists !== null){
            setRows(generateRowsRatings())
        }

    }, [playlist, presetPlaylists]);


    const generateRowsRatings = () => {
        let rowRatings = [];

        playlist.forEach((item, i) => {
            const rIndex = rowRatings.findIndex(r => r.player === item.assignedTo) 
            item.votes.forEach((vote, v) => {
                if(rIndex === -1){
                    rowRatings.push({
                        player: item.assignedTo,
                        votes: 1,
                        stars: vote.vote
                    })
                }else{
                    rowRatings[rIndex].votes += 1;
                    rowRatings[rIndex].stars = (rowRatings[rIndex].stars + vote.vote) / rowRatings[rIndex].votes;
                }
            });
        });

        presetPlaylists.forEach((presetPlaylist, ipp) => {
            presetPlaylist.items.forEach((item, i) => {
                const rIndex = rowRatings.findIndex(r => r.player === item.assignedTo) 
                item.votes.forEach((vote, v) => {
                    if(rIndex === -1){
                        rowRatings.push({
                            player: item.assignedTo,
                            votes: 1,
                            stars: vote.vote
                        })
                    }else{
                        rowRatings[rIndex].votes += 1;
                        rowRatings[rIndex].stars = (rowRatings[rIndex].stars + vote.vote) / rowRatings[rIndex].votes;
                    }
                });
            });
        });

        let output = []
        rowRatings.forEach((row, i) => {
            output.push(createData(row.player, row.votes, row.stars))
        })

        return output.sort((a, b) => b.stars - a.stars)
    }

    const generateStarRating = (rating) => {
        const fullStars = Math.floor(rating);
        const hasHalfStar = rating % 1 !== 0;
        const emptyStars = 5 - fullStars - (hasHalfStar ? 1 : 0);
        const stars = [];
        for (let i = 0; i < fullStars; i++) {
            stars.push(<StarIcon style={{color:'yellow'}} key={`full-${i}`} />);
        }
        if (hasHalfStar) {
            stars.push(<StarHalfIcon style={{color:'yellow'}} key="half" />);
        }
        for (let i = 0; i < emptyStars; i++) {
            stars.push(<StarBorderIcon key={`empty-${i}`} />);
        }
        return stars;
    };

    const generateVotesTable = (rowsResult) => {
        return (
            <TableContainer component={Paper}>
        <Table  aria-label="simple table">
            <TableHead>
            <TableRow>
                <StyledTableCell>Participante</StyledTableCell>
                <StyledTableCell align="right">Votos</StyledTableCell>
                <StyledTableCell align="right">Puntaje</StyledTableCell>
                <StyledTableCell align="right"></StyledTableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {rowsResult?.map((row) => (
                <TableRow key={row.player} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                <TableCell component="th" scope="row">{row.player}</TableCell>
                <TableCell align="right">{row.votes}</TableCell>
                <TableCell align="right">{generateStarRating(row.stars)}</TableCell>
                <TableCell align="right"  style={{color:'yellow'}}>{row.stars}</TableCell>
                </TableRow>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
        )
    }






















    const QRCodeUrl = process.env.REACT_APP_BASE_URL + '/monitor' + queryQR

    console.log(QRCodeUrl)
    return <BootstrapDialog  PaperComponent={PaperComponent} onClose={onClose} aria-labelledby="draggable-dialog-title" open={open}>
      
                {type === 'QR' &&
                    <>
                        <DialogTitle sx={{ m: 0, p: 2 }} style={{ cursor: 'move' }} id="draggable-dialog-title">Escanea el código!</DialogTitle>
                        <IconButton aria-label="close" onClick={onClose} sx={{position: 'absolute', right: 8, top: 8,color: (theme) => theme.palette.grey[500],}}><CloseIcon /></IconButton>
                        <DialogContent dividers>
                        <Typography variant="body2">
                            <QRCode value={QRCodeUrl} />
                        </Typography>
                        </DialogContent>
                    </>
                }


                {type === 'Message' && 
                    <>
                        <DialogTitle sx={{ m: 0, p: 2 }} style={{ cursor: 'move' }} id="draggable-dialog-title">ㅤ</DialogTitle>
                        <IconButton aria-label="close" onClick={onClose} sx={{position: 'absolute', right: 8, top: 8,color: (theme) => theme.palette.grey[500],}}><CloseIcon /></IconButton>
                        <DialogContent dividers>
                        <Typography fontSize={100} align='center'>
                            {message}
                        </Typography>
                        </DialogContent>
                    </>
                }

                {type === 'Votes' && 
                    <>
                        <DialogTitle sx={{ m: 0, p: 2 }} style={{ cursor: 'move' }} id="draggable-dialog-title">Resultados</DialogTitle>
                        <IconButton aria-label="close" onClick={onClose} sx={{position: 'absolute', right: 8, top: 8,color: (theme) => theme.palette.grey[500],}}><CloseIcon /></IconButton>
                        <DialogContent dividers>
                        <Typography fontSize={100} align='center'>
                            {rows !== null && generateVotesTable(rows)}
                        </Typography>
                        </DialogContent>
                    </>
                }

            </BootstrapDialog>

}