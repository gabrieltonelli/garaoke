import React, { useEffect, useState } from 'react';
import axios from 'axios'
import {
    Card, CardContent, FormControlLabel, Checkbox, InputAdornment, IconButton, TextField
} from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';

export default function SearchCard({setKeySearch, setVideosSearchResponse, keySearch, onIsKaraokeOnly, isKaraokeOnly, setSnackMessage, userSettings, selectedApiKey, setSelectedApiKey}) {
  const [error, setError] = useState('');
  const getYoutubeVideos = async (searchQuery, maxResults = 15, apiKeyByParameter = '') => {
    try {
      const finalApiKey = apiKeyByParameter ? apiKeyByParameter : selectedApiKey;
      const apiKey = finalApiKey;
      const apiUrl = process.env.REACT_APP_YOUTUBE_API_URL;
      //all parametters in: https://developers.google.com/youtube/v3/docs/search/list?hl=es-419
      return await axios.get(apiUrl, {
        params: {
          q: isKaraokeOnly? searchQuery + ' karaoke' : searchQuery,
          part: 'snippet',
          key: apiKey,
          maxResults: maxResults,
          type: 'video',
          videoEmbeddable: true,

        },
      }).then((r) =>{
        setVideosSearchResponse(r.data.items);
        setError('')

        setSelectedApiKey(selectedApiKey)
        return true
      }).catch((e)=>{
        setVideosSearchResponse([]);
        setSelectedApiKey(selectedApiKey)
        
        switch(e.code){
          case 'ERR_BAD_REQUEST':
            let actionVerb = 'establecer'
            if(userSettings.configs.apiKeys.filter((apiKey) => apiKey).length > 0) actionVerb = 'cambiar'
            setError(`Es probable que tengas que ${actionVerb} tus API Key para que el buscador basado en Youtube funcione. Ve a la sección de configuración, pestaña 'API Keys de Youtube' para realizar esta acción.`);
            break;
          default:
            setError('Error desconocido: ' + e.message);
        }
        console.error(error)
        return false
      });
    } catch (error) {
      setVideosSearchResponse([]);
      console.error('error=',error)
      setError('Error al obtener videos de YouTube:'+ error.message);
      setSnackMessage({text: 'Error al obtener videos de YouTube:'+ error.message , type: 'error', duration: 10000 })
      return false
      //throw error;
    }
  };

  const getLastApiKeyWorkIndex = (selectedApiKey, apiKeys) => {
    return apiKeys.indexOf(selectedApiKey)
  }

  const handlerSearch = () => {
    const search = document.getElementById('inputSearch').value
    setKeySearch(search)
    let apiKeyWork = false; 
    const apiKeys= userSettings.configs.apiKeys.filter((apiKey) => apiKey)
    const lastApiKeyWorkIndex = selectedApiKey === process.env.REACT_APP_DEFAULT_YOUTUBE_API_KEY ? -1 : getLastApiKeyWorkIndex(selectedApiKey, apiKeys)
    let tryCount = lastApiKeyWorkIndex === -1 ? 0 : lastApiKeyWorkIndex
    let apisCount = apiKeys.length
    let isFirstLoop = true

    if(!apiKeyWork && tryCount===0 && apisCount===0){
      //dont have apiKeys configured
      setSelectedApiKey(process.env.REACT_APP_DEFAULT_YOUTUBE_API_KEY)
      getYoutubeVideos(search, process.env.REACT_APP_YOUTUBE_MAX_RESULTS, process.env.REACT_APP_DEFAULT_YOUTUBE_API_KEY).then((apiKeyWork2) => {
        if(!apiKeyWork2){
          setSnackMessage({text: error, type: 'error', duration: 10000 })
        }
      }
    )
    }else{
      while(!apiKeyWork && tryCount < apisCount){
        setSelectedApiKey(apiKeys[tryCount])
        getYoutubeVideos(search, process.env.REACT_APP_YOUTUBE_MAX_RESULTS, apiKeys[tryCount]).then(apiKeyWork => {
            if(!apiKeyWork){
              setSelectedApiKey(process.env.REACT_APP_DEFAULT_YOUTUBE_API_KEY)
              //intentar con la apikey por defecto
              getYoutubeVideos(search, process.env.REACT_APP_YOUTUBE_MAX_RESULTS, process.env.REACT_APP_DEFAULT_YOUTUBE_API_KEY).then((apiKeyWork2) => {
                  if(!apiKeyWork2){
                    setSnackMessage({text: error, type: 'error', duration: 10000 })
                  }
                }
              )
            }
          }
        )
        tryCount++;
        if(lastApiKeyWorkIndex !==-1 && tryCount === apisCount && isFirstLoop) {
          tryCount = 0;
          apisCount = lastApiKeyWorkIndex;
          isFirstLoop = false;
        }
      }
    }
  };

  const handlerIsKaraokeOnly = (checked) => {
    onIsKaraokeOnly(checked)
  };

  useEffect(() => {
    if( keySearch )
      document.getElementById('inputSearch').value = keySearch
  }, [keySearch]);

  return (<Card elevation={3} style={{ padding: '16px', margin: '10px' }}>
            <CardContent>
              <TextField
                label="Búsqueda"
                placeholder="Escribe el título de la canción aquí"
                id='inputSearch'
                variant="outlined"
                fullWidth
                margin="normal"
                onKeyDown={(event) => {if (event.key === 'Enter'){handlerSearch()}}}
                InputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      <IconButton onClick={handlerSearch} edge="end">
                        <SearchIcon />
                      </IconButton>
                    </InputAdornment>
                  ),
                }}
              />
              <FormControlLabel
                control={<Checkbox color="primary" onChange={(event) => handlerIsKaraokeOnly(event.target.checked)} />}
                label="Buscar solo Karaokes" checked={isKaraokeOnly}
              />
            </CardContent>
          </Card>
          );
}