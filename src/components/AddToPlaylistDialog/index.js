import React from 'react';
import CloseIcon from '@mui/icons-material/Close';
import {
    IconButton, Dialog, DialogTitle, DialogContent,
    Typography,
    InputAdornment,
    TextField,
    Button,
    Grid,
} from '@mui/material';
import { styled } from '@mui/material/styles';
import { AccountCircle } from '@mui/icons-material';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

export default function AddToPlaylistDialog({ show, video, close, addToPlayList }) {

    const handleAddToPlayList = (video) => {
        addToPlayList(video, document.getElementById('assignedTo').value)
        close()   
    }

    return <BootstrapDialog
        onClose={close}
        aria-labelledby="customized-dialog-title"
        open={show}

    >
        <DialogTitle sx={{ m: '0', p: 2 }} id="customized-dialog-title" >
            Agregar a lista de reproducción
        </DialogTitle>
        <IconButton
            aria-label="close"
            onClick={close}
            sx={{
                position: 'absolute',
                right: 8,
                top: 8,
                color: (theme) => theme.palette.grey[500],
            }}
        >
            <CloseIcon />
        </IconButton>
        <DialogContent dividers >
            <Grid container direction="row" rowSpacing={4} columnSpacing={{ xs: 1, sm: 2, md: 3 }} justifyContent="space-around" alignItems="center">
                <Grid item xs={12}>
                    {video &&
                        <Typography gutterBottom style={{ color: '#36FF00', textAlign: 'center', fontSize: 18, fontWeight: 'bold', marginBottom: 40 }}>
                            <img alt="Preview miniatura del video" src={video.snippet.thumbnails.default.url} /><br />
                            {video.snippet.title}
                        </Typography>
                    }
                </Grid>
                <Grid item xs={12}>
                    <TextField
                        fullWidth
                        id="assignedTo"
                        label="Asignar a"
                        onKeyDown={(event) => {if (event.key === 'Enter'){event.preventDefault();handleAddToPlayList(video);}}}
                        InputProps={{
                            maxLength: 20,
                            startAdornment: (
                                <InputAdornment position="start">
                                    <AccountCircle />
                                </InputAdornment>
                            ),
                        }}
                        variant="outlined"
                    />
                </Grid>
                <Grid item xs={6} style={{textAlign: 'center'}} >
                    <Button variant="outlined" id="addToPlaylistButton" startIcon={<PlaylistAddIcon />} onClick={() => handleAddToPlayList(video)}>
                        Agregar
                    </Button>
                </Grid>
                <Grid item xs={6} style={{textAlign: 'center'}} >
                    <Button variant="outlined" startIcon={<CloseIcon />} onClick={() => close()}>
                        Cancelar
                    </Button>
                </Grid>
            </Grid>
        </DialogContent>
    </BootstrapDialog>

}