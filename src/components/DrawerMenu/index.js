import React from 'react';
import {
    Drawer, List, ListItem, ListItemIcon, ListItemText
  } from '@mui/material';
import { Link } from 'react-router-dom';
import Home from '@mui/icons-material/Home';
import Explore from '@mui/icons-material/Explore';
import Settings from '@mui/icons-material/Settings';
import Person from '@mui/icons-material/Person';
export default function DrawerMenu({show, close}) {
    const drawerItems = [
      { text: 'Configuraciones', icon: <Home />, url: '/config' },
      { text: 'Explore', icon: <Explore />, url: '/explore' },
      { text: 'Settings', icon: <Settings />, url: '/settings' },
      { text: 'Profile', icon: <Person />, url: '/profile' },
    ];

    return <Drawer anchor="right" open={show} onClose={close}>
          <List>
          {drawerItems.map((item, index) => (
            <ListItem key={index} component={Link} to={item.url} onClick={close}>
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.text} />
            </ListItem>
          ))}
          </List>
        </Drawer>
  }