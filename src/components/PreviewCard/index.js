import React from 'react';
import {
    Card, CardContent, Grid, Typography,
} from '@mui/material';
import YouTube from 'react-youtube';

export default function PreviewCard({videoId = null}) {


    const opts = {
        height: '200px',
        width: '350px',
        playerVars: {
          // https://developers.google.com/youtube/player_parameters
            mute: true,
            autoplay: true,
        },
      };

    return (<Card elevation={3} style={{ padding: '16px 0 0 16px', margin: '10px' }}>
                <CardContent>
                  <Grid container spacing={2} >
                    <Grid item>
                      <Typography variant="h6" component="div" style={{marginBottom:'10px'}}>
                        Previsualizador
                      </Typography>
                    </Grid>
                    <Grid container item justifyContent="center">
                    <div style={{height: 200, width: 350, border:'1px solid #555', textAlign:'center', display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center', 
                    color:'#555'}}>
                    {videoId? 
                    <YouTube videoId={videoId} opts={opts} style={{display:'contents'}} />
                    :
                        'Nada para previsualizar'
                    }
                    </div>
                    </Grid>

                  </Grid>
                </CardContent>
              </Card>);
}             