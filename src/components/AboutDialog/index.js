import React, { memo } from 'react';
import CloseIcon from '@mui/icons-material/Close';
import {
  IconButton, Dialog, DialogTitle, DialogContent,
  Typography,
} from '@mui/material';
import { styled } from '@mui/material/styles';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
  '& .MuiDialogContent-root': {
    padding: theme.spacing(2),
  },
  '& .MuiDialogActions-root': {
    padding: theme.spacing(1),
  },
}));

const AboutDialog = ({ show, close }) => {
  return (
    <BootstrapDialog
      onClose={close}
      aria-labelledby="customized-dialog-title"
      open={show}
    >
      <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
        Acerca de Garaoke
      </DialogTitle>
      <IconButton
        aria-label="close"
        onClick={close}
        sx={{
          position: 'absolute',
          right: 8,
          top: 8,
          color: (theme) => theme.palette.grey[500],
        }}
      >
        <CloseIcon />
      </IconButton>
      <DialogContent dividers>
        <Typography gutterBottom>
          Este proyecto fue creado para llevar la máxima diversión a tu fiesta de un modo simple e intuitivo, sin instalaciones complicadas y sin costos, ¡GRATIS! para todos los amantes del Karaoke.
          <br />Desarrollado por Gabriel Tonelli - gabrieltonelli@gmail.com
        </Typography>
        <Typography gutterBottom>
          <ul>
            <li>Versión: 1.2.0</li>
            <li>Fecha: 24/01/2024</li>
            <li>Origen: Chacabuco, Buenos Aires Argentina</li>
          </ul>
        </Typography>
      </DialogContent>
    </BootstrapDialog>
  );
};

export default memo(AboutDialog);
