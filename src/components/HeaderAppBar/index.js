import React, { useCallback, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import {
  AppBar, Toolbar, IconButton, 
  Box, Tooltip, Avatar, 
  Popover, Typography, Button,
  TextField, InputAdornment, Select, MenuItem
} from '@mui/material';
import SettingsIcon from '@mui/icons-material/Settings';
import TvIcon from '@mui/icons-material/Tv';
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import GridOnIcon from '@mui/icons-material/GridOn';
import QrCodeIcon from '@mui/icons-material/QrCode';
import MessageIcon from '@mui/icons-material/Message';
import SpeakerNotesOffIcon from '@mui/icons-material/SpeakerNotesOff';
import InterpreterModeIcon from '@mui/icons-material/InterpreterMode';
import AboutDialog from '../AboutDialog';
import { useLocation } from 'react-router-dom';

export default function HeaderAppBar({
  user, 
  openVideoPlayer, 
  setCurrentIntroVideo,
  setCurrentTransitionVideo,
  currentIntroVideo, 
  currentTransitionVideo, 
  showHideModalQR, 
  showHideModalMessage, 
  videoPlayer,
  userSettings,
  handleGoogleSignOut,
  allowInteractivity,
  setVideoId,
  showModalMessage,
  showHideModalVotesTable
}) {
  const location = useLocation();
  const [openAboutModal, setOpenAboutModal] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [reExecute, setReExecute] = useState(null);
  const [showingScreen, setShowingScreen] = useState(false);
  const isConfigPage = location.pathname === '/config';

  const daemon = useCallback(() => {
    console.log('Header Daemon');
    setReExecute(Date());
  }, []);

  useEffect(() => {
    if (videoPlayer && videoPlayer.name === 'videoPlayer') {
      setShowingScreen(true);
    } else {
      setShowingScreen(false);
      setVideoId(null)

    }
  }, [videoPlayer, reExecute, setVideoId]);

  useEffect(() => {
    const inter = setInterval(daemon, 4000);
    return () => clearInterval(inter);
  }, [daemon]);

  const handleCloseAboutModal = () => {
    setOpenAboutModal(false);
  };

  const handleAboutClick = () => {
    setOpenAboutModal(true);
  };

  const userMenuOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const userMenuClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  const setAndShowHideModalMessage = () => {
    const message = document.getElementById('modalMessage').value
    showHideModalMessage(message)
  }

  const handleChangeIntro = (e) => {
    setCurrentIntroVideo(e.target.value)
  }

  const handleChangeTransition = (e) => {
    setCurrentTransitionVideo(e.target.value)
  }

  const getIntros = () => {
    return (
    userSettings.configs.intros.map((intro, index) => {
      if(intro){
        return <MenuItem key={intro} value={intro}>Mi Intro {(index + 1)}</MenuItem>
      }
      return ''
    })
    )
  }

  const getTransitions = () => {
    return (
    userSettings.configs.transitions.map((transition, index) => {
      if(transition){
        return <MenuItem key={transition} value={transition}>Mi Transición {(index + 1)}</MenuItem>
      }
      return ''
    })
    )
  }

  const getTooltipQRButton = () => {
    let tooltip = 'Mostrar / ocultar QR de Apareamiento.'
    if (!allowInteractivity) {
      tooltip += ' Para activar esta función es necesario que se encuentre habilitado [Permitir monitor de interactividad] desde la configuración.'
    }
    return tooltip
  }

  const getTooltipVotesTable = () => {
    let tooltip = 'Mostrar / ocultar Tabla de Resultados.'
    if (!allowInteractivity) {
      tooltip += ' Para activar esta función es necesario que se encuentre habilitado [Permitir monitor de interactividad] desde la configuración.'
    }
    return tooltip
  }

  

  return (
      <>
        <AppBar position="static">
          <Toolbar>
            <InterpreterModeIcon style={{ cursor: 'pointer', marginRight: 10, fontSize: 30 }} onClick={handleAboutClick} />
            <h2 onClick={handleAboutClick} style={{ cursor: 'pointer', fontWeight: 200, width: 270}}>
              Garaoke
            </h2>
            {!isConfigPage &&
            <>

            <Box sx={{ display: { xs: 'none', sm: 'block' }, alignItems: 'center', textAlign: 'center', width: '100%' }}>

              <Tooltip title="Abrir pantalla de reproducción">
                <IconButton color="inherit" onClick={() => openVideoPlayer()} style={{margin: '0 40px 0 0'}}>
                  <TvIcon/>
                </IconButton>
              </Tooltip>
        
              <Select 
                labelId="select-intro"
                id="select-intro"
                value={currentIntroVideo}
                onChange={handleChangeIntro}
                size="small"
                style={{width: 200}}
              >
                <MenuItem value={process.env.REACT_APP_DEFAULT_INTRO_VIDEO_CODE}>Intro por defecto</MenuItem> 
                {getIntros()}
              </Select>
              <Tooltip title="Reproducir intro">
                <IconButton color="inherit" onClick={() => openVideoPlayer(currentIntroVideo)} style={{margin: '0 40px 0 5px'}}>
                  <PlayArrowIcon/>
                </IconButton>
              </Tooltip>

              <Select 
                labelId="select-transition"
                id="select-transition"
                value={currentTransitionVideo}
                onChange={handleChangeTransition}
                size="small"
                style={{width: 200}}
              >
                <MenuItem value={process.env.REACT_APP_DEFAULT_TRANSITION_VIDEO_CODE}>Transición por defecto</MenuItem> 
                {getTransitions()}
              </Select>
              <Tooltip title="Reproducir transición">
                <IconButton color="inherit" onClick={() => openVideoPlayer(currentTransitionVideo)} style={{margin: '0 40px 0 5px'}}>
                  <PlayArrowIcon/>
                </IconButton>
              </Tooltip>

              <TextField
                        placeholder="Establece el mensaje..."
                        id='modalMessage'
                        variant="outlined"
                        size="small"
                        disabled={!videoPlayer || videoPlayer.closed}
                        margin="normal"
                        style={{marginTop:0, color: '#ccc'}}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              
                                <IconButton onClick={() => setAndShowHideModalMessage()} edge="end" disabled={!showingScreen}>
                                  {showModalMessage?
                                    <Tooltip title="Quitar el mensaje de la pantalla"><SpeakerNotesOffIcon/></Tooltip>
                                    :
                                    <Tooltip title="Enviar el mensaje a la pantalla"><MessageIcon/></Tooltip>
                                  }
                                </IconButton>
                              
                            </InputAdornment>
                          ),
                        }}
              />

              <Tooltip title={getTooltipQRButton()}>
                <span>
                  <IconButton color="inherit" onClick={() => showHideModalQR()} disabled={!showingScreen} style={{margin: '0 0 0 40px'}}>
                    <QrCodeIcon/>
                  </IconButton>
                </span>
              </Tooltip>

              <Tooltip title={getTooltipVotesTable()}>
                <span>
                  <IconButton color="inherit" onClick={() => showHideModalVotesTable()} disabled={!showingScreen} style={{margin: '0 0 0 30px'}}>
                    <GridOnIcon/>
                  </IconButton>
                </span>
              </Tooltip>

            </Box>

            <div style={{ marginLeft: '10', display: 'flex', justifyContent: 'flex-end' }}>
              
              <Box sx={{textWrap: 'nowrap', padding: '10px 0', cursor: 'pointer', alignItems: 'center'}} display='flex' onClick={userMenuOpen}><span style={{color:'#555', marginRight:6}}>⯆</span> {user.displayName}
              <Avatar alt="Avatar" src={user.photoURL} style={{margin: '0 10px'}} />
              </Box>
              <Popover
                id='userMenu'
                open={open}
                anchorEl={anchorEl}
                onClose={userMenuClose}
                anchorOrigin={{
                  vertical: 'bottom',
                  horizontal: 'left',
                }}
              >
                <Typography sx={{ p: 2 }}>
                  <Button sx={{textTransform: 'inherit'}} onClick={() => handleGoogleSignOut()}>
                    Salir y cerrar sesión
                  </Button> 
                </Typography>
              </Popover>

              <Link to="/config" style={{ textDecoration: 'none', color: 'inherit', padding: '10px 0' }}>
                <IconButton color="inherit">
                  <SettingsIcon/>
                </IconButton>
              </Link>
            </div>
            </>
          }
          </Toolbar>
        </AppBar>
        <AboutDialog show={openAboutModal} close={handleCloseAboutModal} />
        {/* <DrawerMenu show={openDrawer} close={handleDrawerClose} />*/}
      </>
      );
}