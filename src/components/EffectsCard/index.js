import React, { useEffect, useState, useCallback, memo, useMemo } from 'react';
import EffectButton from './components/EffectButton';
import {
  Card, CardContent, 
  Grid, 
  Typography,
  Stack, Slider, Box, TextField, MenuItem 
} from '@mui/material';
import VolumeDown from '@mui/icons-material/VolumeDown';
import VolumeUp from '@mui/icons-material/VolumeUp';
import EFFECTS from '../../constants';

const EffectsCard = () => {
  const [volume, setVolume] = useState(0.5);
  const [keySearch, setKeySearch] = useState('');
  const [effectsFiltered, setEffectsFiltered] = useState(EFFECTS);
  const [selectedCategory, setSelectedCategory] = useState('- Todas las etiquetas -');

  const categories = useMemo(() => {
    return Array.from(new Set(EFFECTS.flatMap(effect => effect.categories.map(category => category.name))));
  }, []);

  const handleCategoryChange = useCallback((event) => {
    setSelectedCategory(event.target.value);
    let effectsAux = keySearch.length > 0 ?
      EFFECTS.filter((effect) => {
        const effectName = effect.name.toUpperCase();
        const keyToSearch = keySearch.toUpperCase();
        return effectName.includes(keyToSearch);
      })
      :
      EFFECTS;

    if (event.target.value !== '- Todas las etiquetas -') {
      setEffectsFiltered(effectsAux.filter((effect) => effect.categories.some(category => category.name === event.target.value)));
    } else {
      setEffectsFiltered(effectsAux);
    }
  }, [keySearch]);

  const handleVolumeChange = useCallback((event, newValue) => {
    setVolume(newValue / 100);
  }, []);

  const handleEffectsSearch = useCallback((event) => {
    setKeySearch(event.target.value);
  }, []);

  const filterByKey = useCallback((keySearch) => {
    if (keySearch.length > 0) {
      const filtered = EFFECTS.filter((effect) => {
        const effectName = effect.name.toUpperCase();
        const keyToSearch = keySearch.toUpperCase();
        return effectName.includes(keyToSearch);
      });
      setEffectsFiltered(filtered);
    } else {
      setEffectsFiltered(EFFECTS);
    }
    const event = { target: { value: selectedCategory } };
    handleCategoryChange(event);
  }, [handleCategoryChange, selectedCategory]);

  useEffect(() => {
    filterByKey(keySearch);
  }, [keySearch, filterByKey]);

  return (
    <Card elevation={3} style={{ padding: '16px', margin: '10px' }} sx={{ display: { xs: 'none', sm: 'block' } }}>
      <CardContent>
        <Grid container direction="row" justifyContent="space-evenly">
          <Grid item xs={2} md={2}>
            <Typography variant="h6" component="div">
              Efectos
            </Typography>
          </Grid>
          <Grid item xs={6} md={6}>
            <TextField
              placeholder="Buscar efectos"
              onChange={handleEffectsSearch}
              variant="outlined"
              fullWidth
              size="small"
              margin="normal"
              style={{ marginTop: 0 }}
            />
          </Grid>
          <Grid item xs={4} md={4} style={{ paddingLeft: 10 }}>
            <TextField
              select
              value={selectedCategory}
              onChange={handleCategoryChange}
              variant="outlined"
              fullWidth
              size="small"
              margin="normal"
              defaultValue=""
              style={{ marginTop: 0 }}
            >
              <MenuItem key={'todas'} value={'- Todas las etiquetas -'}>
                - Todas las etiquetas -
              </MenuItem>
              {categories.map((category) => (
                <MenuItem key={category} value={category}>
                  {category}
                </MenuItem>
              ))}
            </TextField>
          </Grid>
          <div id="effectsList" className="effects_container scroll" style={{ marginBottom: 30 }}>
            {effectsFiltered.map((effect) => (
              <EffectButton key={effect.id} effect={effect} volume={volume} setCategory={handleCategoryChange} />
            ))}
          </div>
          <Box sx={{ width: '80%' }}>
            <Stack spacing={2} direction="row" sx={{ mb: 1 }} alignItems="center">
              <VolumeDown />
              <Slider aria-label="Volume" value={volume * 100} onChange={handleVolumeChange} />
              <VolumeUp />
            </Stack>
          </Box>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default memo(EffectsCard);
