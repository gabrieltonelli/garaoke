import React, { useRef, useEffect, useState, useCallback } from "react";
import { Howl } from 'howler';
import ReactHowler from 'react-howler';
import { Card, CardContent, Chip, Grid, IconButton, Stack, Tooltip, Typography } from "@mui/material";
import PlayArrowIcon from '@mui/icons-material/PlayArrow';
import StopIcon from '@mui/icons-material/Stop';

const baseAssetsPath = '../../../../assets/';

export default function EffectButton({ effect, volume, setCategory }) {
  const [isPlaying, setIsPlaying] = useState(false);
  const soundRef = useRef(null);
  const loadAudio = useCallback(() => {
    const sound = new Howl({
      src: [`${baseAssetsPath}${effect.path}`],
      volume: volume,
      onend: () => {
        console.log('Finished playing');
        setIsPlaying(false)
      },
      onloaderror: (id, error) => {
        console.error(`Error loading audio ${effect.path}:`, error);
      }
    });

    soundRef.current = sound;
  },[volume, effect.path]);

  useEffect(() => {
    loadAudio();
  }, [effect.path, loadAudio]);

  const playAudio = () => {
    if (soundRef.current) {
        if (!isPlaying){
            setIsPlaying(true);  
            soundRef.current.play();
        }else{
            setIsPlaying(false);
            soundRef.current.stop();
        }
    }
  };

  return (
    <Card elevation={3} style={{ margin: '10px 0' }}>
        <CardContent>
            <Grid container spacing={2}>
                <Grid item xs={10} md={10}>
                    <Typography variant="body1" component="div">
                        {effect.name}
                    </Typography>
                    <Stack direction="row" spacing={1}>
                      {effect.categories.map((cat) => (
                        <Chip size="small" key={cat.name} label={cat.name} color="primary" variant="outlined" onClick={()=>setCategory({ target:{value: cat.name }})}/>
                      ))}
                    </Stack>
                </Grid>
                <Grid item xs={2} md={2} style={{ textAlign: 'right', marginLeft: 'auto' }}>
                    <Tooltip title={isPlaying ? "Detener efecto" : "Reproducir efecto"}>
                        <IconButton color="inherit" onClick={playAudio}>
                            {isPlaying ? <StopIcon /> : <PlayArrowIcon />}
                        </IconButton>
                    </Tooltip>
                </Grid>
            </Grid>



            <ReactHowler
                src={`${baseAssetsPath}${effect.path}`}
                playing={false}
                volume={0.5}
            />
        </CardContent>
    </Card>
      
      

  );
}
