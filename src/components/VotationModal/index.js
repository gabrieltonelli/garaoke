import React, { useCallback, useEffect, useState } from 'react';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import StarIcon from '@mui/icons-material/Star';

import {
    IconButton, Dialog, DialogTitle, DialogContent,
    Typography,
    Box,
  } from '@mui/material';
import { styled } from '@mui/material/styles';
import Paper from '@mui/material/Paper';


const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));


function PaperComponent(props) {
    return (
        <Paper {...props} />
    );
}

export default function VotationModal({
    targetVideo, 
    currentVideoData, 
    setShowVotation, 
    updateUserField, 
    userId,
    stars,
    setStars
    }) {
    
    const [countdown, setCountdown] = useState(10);
    let theInterval = null;
    let count = 10;

    /* eslint-disable */

    useEffect(() => {
        theInterval = setInterval(countdownDecrease, 1000)
      }, []);

    const countdownDecrease = useCallback(() => {
        if(count <= 0){
            clearInterval(theInterval)
            setShowVotation(false)
        }else{
            count -= 1
            setCountdown(count)
        }
    }, [stars]);

    /* eslint-enable */

    return <BootstrapDialog  PaperComponent={PaperComponent} open={true}>
            <DialogTitle sx={{ m: 0, p: 2 }}>{`Que te ha parecido ${currentVideoData && currentVideoData.assignedTo} ?`}</DialogTitle>
            <DialogContent dividers>
                
                {countdown !== null &&
                    <Box display="flex" justifyContent="center" alignItems="center" style={{color: '#888'}}>
                        Vota antes de  
                        <Typography variant="h5" style={{textAlign: 'center', marginRight: 5, marginLeft: 5, color: '#888'}}>
                            {countdown} 
                        </Typography>
                        seg.
                    </Box>
                }
                <Box display="flex" justifyContent="center" alignItems="center" style={{marginTop: 20, marginBottom: 20}}>
                    <IconButton onClick={()=>setStars(stars === 1 ? 0 : 1)}>{stars > 0 ? <StarIcon style={{color:'yellow'}}/> : <StarBorderIcon/>}</IconButton>
                    <IconButton onClick={()=>setStars(2)}>{stars > 1 ? <StarIcon style={{color:'yellow'}}/> : <StarBorderIcon color={'yellow'}/>}</IconButton>
                    <IconButton onClick={()=>setStars(3)}>{stars > 2 ? <StarIcon style={{color:'yellow'}}/> : <StarBorderIcon/>}</IconButton>
                    <IconButton onClick={()=>setStars(4)}>{stars > 3 ? <StarIcon style={{color:'yellow'}}/> : <StarBorderIcon/>}</IconButton>
                    <IconButton onClick={()=>setStars(5)}>{stars > 4 ? <StarIcon style={{color:'yellow'}}/> : <StarBorderIcon/>}</IconButton>
                </Box>

            </DialogContent>
            </BootstrapDialog>

}