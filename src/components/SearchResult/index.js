import React, { useEffect, useState } from 'react';
import {Box, Card, CardContent, Grid, IconButton, Tooltip, Typography} from '@mui/material';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import SmartDisplayIcon from '@mui/icons-material/SmartDisplay';
import RemoveRedEyeIcon from '@mui/icons-material/RemoveRedEye';
import AddToPlaylistDialog from '../AddToPlaylistDialog';

export default function SearchResult({ 
  videos, 
  setPreviewVideoId, 
  openVideoPlayer, 
  setPlaylist, 
  playlist, 
  setReloadPlaylist,
  currentPlaylist,
  setCurrentPlaylistItem,
  videoId
}) {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  const [openAddToPlaylistModal, setOpenAddToPlaylistModal] = useState(false);
  const [videoToAdd, setVideoToAdd] = useState(null);

  const handleCloseAddToPlaylistModal = () => {
    console.log('entra a cerrar dialog')
    setOpenAddToPlaylistModal(false);
  };

  const addToPlayList = (videoObj, assignedTo = '') => {
    console.log('Do addToPlayList() videoObj=', videoObj)
    const newPlaylist = playlist;
    const videoItem = {
      id: (getLastVideoId() + 1).toString(),
      title: videoObj.snippet.title,
      videoId: videoObj.id.videoId,
      thumbnail: videoObj.snippet.thumbnails.default.url,
      played: false,
      assignedTo: assignedTo,
      votes: [],
    }
    newPlaylist.push(videoItem)
    setPlaylist(newPlaylist)
    setReloadPlaylist(new Date())
  } 

  const getLastVideoId = () => {
    const lastAssignedId = playlist.reduce((maxId, item) => {
      const currentId = parseInt(item.id, 10);
      return currentId > maxId ? currentId : maxId;
    }, 0);
    return lastAssignedId;
  }

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const videoPlayNow = (videoId) => {
    openVideoPlayer(videoId);
  }

  const handleAddToPlayList = (video) =>{
    setOpenAddToPlaylistModal(true)
    setVideoToAdd(video)
  }

  const playNow = (item) => {
    setCurrentPlaylistItem({ playlist: null, item: item.id})
    console.log('playNow item=', item)
    videoPlayNow(item.videoId);
  }

  const getColorItem = (item) => {
    if (videoId === item.videoId) {
      return '#00FF11';
    } 
    return '#fff';
    
  }

  return (
  <>
  <AddToPlaylistDialog show={openAddToPlaylistModal} video={videoToAdd} close={handleCloseAddToPlaylistModal} addToPlayList={addToPlayList}/>
  <Card elevation={3} style={{ padding: '16px', margin: windowWidth > 900 ? '10px 0' : '0 10px' }} >
    <CardContent>
      <Grid container spacing={2}>
        <Grid item xs={8} md={8}>
          <Typography variant="h6" component="div">
            Resultados de búsqueda
          </Typography>
        </Grid>
      </Grid>
      <div id="playList" class="video_search_container scroll">
        {videos.map((video) => (
          <Card elevation={3} style={{ margin: '10px 0' }}>
            <CardContent>
              <Grid container spacing={2}>
                <Grid item xs={5} md={5}>
                  <img alt="Preview miniatura del video" src={video.snippet.thumbnails.high.url} width={'100%'} />
                </Grid>
                <Grid item xs={7} md={7}>
                  <Typography variant="body1" component="div" sx={{ color: getColorItem(video.id) }}>
                    {video.snippet.title}
                  </Typography>
                  <Box>
                    <Tooltip title="Reproducir ahora" sx={{ display: { xs: 'none', sm: 'block' } }}>
                      <IconButton onClick={() => playNow(video.id)}>
                        <SmartDisplayIcon color='red' />
                      </IconButton> 
                    </Tooltip>
                    <Tooltip title="Vista preliminar">
                      <IconButton onClick={() => setPreviewVideoId(video.id.videoId)}>
                        <RemoveRedEyeIcon />
                      </IconButton>
                    </Tooltip>
                    {currentPlaylist === 'user_playlist' &&
                    <Tooltip title="Enviar a la lista de reproducción">
                      <IconButton onClick={() => handleAddToPlayList(video)}>
                        <PlaylistAddIcon />
                      </IconButton>
                    </Tooltip>
                    }
                  </Box>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        ))
        }
      </div>
    </CardContent>
  </Card>
  </>);
}