import React from 'react';
import Snackbar from '@mui/material/Snackbar';
import { Alert } from '@mui/material';

export default function SnackMessage({message, setMessage, duration = 2000}) {

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setMessage({text:'', type: ''});
  };

  return (
    <div>
      <Snackbar
        open={Boolean(message.text)}
        autoHideDuration={duration}
        onClose={handleClose}
      >
        <Alert
          onClose={handleClose}
          severity={message.type}
          variant="filled"
          sx={{ width: '100%', color: '#fff' }}
        >
          {message.text}
        </Alert>
      </Snackbar>
    </div>
  );
}