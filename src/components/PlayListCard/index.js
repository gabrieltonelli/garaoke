import React, { useEffect, useState, useCallback, memo } from 'react';
import {
  Button,
  Card, CardContent, Checkbox, Chip, Dialog, DialogActions, DialogContent, 
  DialogContentText, DialogTitle, Grid, IconButton, InputAdornment, MenuItem, Select, TextField, Tooltip, Typography,
} from '@mui/material';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import FileUploadIcon from '@mui/icons-material/FileUpload';
import SmartDisplayIcon from '@mui/icons-material/SmartDisplay';
import ClearIcon from '@mui/icons-material/Clear';
import EditIcon from '@mui/icons-material/Edit';
import AccountCircle from '@mui/icons-material/AccountCircle';
import GetAppIcon from '@mui/icons-material/GetApp';

const PlayListCard = ({
  playlist, 
  videoPlayNow, 
  setPlaylist, 
  currentPlaylist, 
  setCurrentPlaylist,
  userPresetPlaylists,
  originPresetPlaylists,
  setOriginPresetPlaylists,
  originPresetPlaylist,
  setOriginPresetPlaylist,
  userPresetPlaylist,
  setUserPresetPlaylist,
  setCurrentPlaylistItem,
  currentPlaylistItem,
  videoId
}) => {
  const [deletionDialogOpen, setDeletionDialogOpen] = useState(false);
  const [deletionItemId, setDeletionItemId] = useState(null);
  const [assignedToDialogOpen, setAssignedToDialogOpen] = useState(false);
  const [assignedToItemId, setAssignedToItemId] = useState(null);
  const [playlistObj, setPlaylistObj] = useState(playlist);

  /* eslint-disable */
  const loadPlaylists = useCallback(async () => {
    const requireContext = require.context('../../playlists', false, /\.txt$/);
    const fileKeys = requireContext.keys();
    const playlists = await Promise.all(
      fileKeys.map(async (filename) => {
        const response = await fetch(requireContext(filename));
        const content = await response.text();
        const name = filename.replace('./', '').replace('.txt', '');
        return { name: name, items: JSON.parse(content) };
      })
    );
    setOriginPresetPlaylists(playlists);
  }, [setOriginPresetPlaylists]);

  useEffect(() => {
    loadPlaylists();
  }, [loadPlaylists]);

  
  useEffect(() => {
    if (currentPlaylist === 'user_playlist') {
      setPlaylistObj(playlist);
    } else {
      const oppl = getOriginPresetPlayListByName(currentPlaylist);
      const uppl = getUserPresetPlayListByName(currentPlaylist);
      setOriginPresetPlaylist(oppl);
      setUserPresetPlaylist(uppl);
      setPlaylistObj(intersectionPresetPlaylist(oppl, uppl));
    }
  }, [playlist, userPresetPlaylist, currentPlaylist]);

  const handleDragEndPlaylist = useCallback((result) => {
    if (!result.destination) return;
    const newPlaylist = Array.from(playlistObj);
    const [reorderedItem] = newPlaylist.splice(result.source.index, 1);
    newPlaylist.splice(result.destination.index, 0, reorderedItem);
    setPlaylist(newPlaylist);
  }, [playlistObj, setPlaylist]);

  const handleCheckVideo = useCallback((checked, itemId) => {
    const updatedPlaylist = playlistObj.map(item => {
      if (item.id === itemId) {
        return { ...item, played: checked };
      }
      return item;
    });
    setPlaylist(updatedPlaylist);
  }, [playlistObj, setPlaylist]);

  const handleDeleteAssignedTo = useCallback((itemId) => {
    const updatedPlaylist = playlistObj.map(item => {
      if (item.id === itemId) {
        return { ...item, assignedTo: '' };
      }
      return item;
    });
    setPlaylist(updatedPlaylist);
  }, [playlistObj, setPlaylist]);

  const handleOpenDeletionDialog = useCallback((itemId) => {
    setDeletionItemId(itemId);
    setDeletionDialogOpen(true);
  }, []);

  const handleOpenEditAssignedToDialog = useCallback((itemId) => {
    setAssignedToItemId(itemId);
    setAssignedToDialogOpen(true);
  }, []);

  const handleCloseDeletionDialog = useCallback(() => {
    setDeletionItemId(null);
    setDeletionDialogOpen(false);
  }, []);

  const handleCloseAssignedToDialog = useCallback(() => {
    setAssignedToItemId(null);
    setAssignedToDialogOpen(false);
  }, []);

  const handleConfirmDeletion = useCallback(() => {
    const newPlaylist = playlistObj.filter(item => item.id !== deletionItemId);
    setPlaylist(newPlaylist);
    handleCloseDeletionDialog();
  }, [deletionItemId, playlistObj, setPlaylist, handleCloseDeletionDialog]);

  const handleConfirmAssignedTo = useCallback(() => {
    const assignedTo = document.getElementById('assignedToFromPlaylist').value;
    const updatedPlaylist = playlistObj.map(item => {
      if (item.id === assignedToItemId) {
        return { ...item, assignedTo: assignedTo };
      }
      return item;
    });
    setPlaylist(updatedPlaylist);
    handleCloseAssignedToDialog();
  }, [assignedToItemId, playlistObj, setPlaylist, handleCloseAssignedToDialog]);

  const handleExport = useCallback(() => {
    const filename = 'garaoke_playlist';
    const text = JSON.stringify(playlist);
    const blob = new Blob([text], { type: 'text/plain' });
    const link = document.createElement('a');
    link.href = URL.createObjectURL(blob);
    link.download = `${filename}.txt`;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }, [playlist]);

  const handleImport = useCallback(() => {
    const input = document.createElement('input');
    input.type = 'file';
    input.accept = '.txt';
    input.onchange = (event) => {
      const file = event.target.files[0];
      const reader = new FileReader();
      reader.onload = (event) => {
        const text = event.target.result;
        const importedPlaylist = JSON.parse(text);
        setPlaylist(importedPlaylist);
      };
      reader.readAsText(file);
    };
    input.click();
  }, [setPlaylist]);

  const handleChangePlaylist = useCallback((e) => {
    setCurrentPlaylist(e.target.value);
    if (e.target.value === 'user_playlist') {
      setPlaylistObj(playlist);
    } else {
      const oppl = getOriginPresetPlayListByName(e.target.value);
      const uppl = getUserPresetPlayListByName(e.target.value);
      setOriginPresetPlaylist(oppl);
      setUserPresetPlaylist(uppl);
      setPlaylistObj(intersectionPresetPlaylist(oppl, uppl));
    }
  }, [playlist, setCurrentPlaylist, setOriginPresetPlaylist, setUserPresetPlaylist]);

  const intersectionPresetPlaylist = useCallback((oppl, uppl) => {
    let userPresetPlaylistItems = [];
    let originPresetPlaylistItems = [];
    uppl.forEach(item => {
      if (oppl.some(op => op.videoId === item.videoId)) {
        userPresetPlaylistItems.push(item);
      }
    });
    oppl.forEach(item => {
      if (!uppl.some(up => up.videoId === item.videoId)) {
        originPresetPlaylistItems.push(item);
      }
    });
    return [...userPresetPlaylistItems, ...originPresetPlaylistItems];
  }, []);

  const getOriginPresetPlayListByName = useCallback((name) => {
    return originPresetPlaylists.find(playlist => playlist.name === name)?.items || [];
  }, [originPresetPlaylists]);

  const getUserPresetPlayListByName = useCallback((name) => {
    return userPresetPlaylists.find(playlist => playlist.name === name)?.items || [];
  }, [userPresetPlaylists]);

  /* eslint-enable */

  const playNow = (item) => {
    setCurrentPlaylistItem({ playlist: currentPlaylist, item: item})
    videoPlayNow(item.videoId);
  }
  const deletionDialog = (
    <Dialog
      open={deletionDialogOpen}
      onClose={handleCloseDeletionDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Confirmar eliminación"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          ¿Estás seguro de que quieres eliminar este elemento?
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseDeletionDialog} color="primary">
          Cancelar
        </Button>
        <Button onClick={handleConfirmDeletion} color="primary" autoFocus>
          Eliminar
        </Button>
      </DialogActions>
    </Dialog>
  );

  const assignedToDialog = (
    <Dialog
      open={assignedToDialogOpen}
      onClose={handleCloseAssignedToDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Asignar video"}</DialogTitle>
      <DialogContent>
        <TextField
          fullWidth
          id="assignedToFromPlaylist"
          label="Asignar a"
          onKeyDown={(event) => {
            if (event.key === 'Enter') {
              event.preventDefault();
              handleConfirmAssignedTo();
            }
          }}
          style={{ marginTop: 20 }}
          InputProps={{
            maxLength: 20,
            startAdornment: (
              <InputAdornment position="start">
                <AccountCircle />
              </InputAdornment>
            ),
          }}
          variant="outlined"
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseAssignedToDialog} color="primary">
          Cancelar
        </Button>
        <Button onClick={handleConfirmAssignedTo} color="primary" autoFocus>
          Asignar
        </Button>
      </DialogActions>
    </Dialog>
  );

  const getColorItem = (item) => {
    console.log('Do getColorItem() videoId=',videoId)
    if (videoId === item.videoId) {
      return '#00FF11';
    } else if (item.played) {
      return '#888';
    } else {
      return '#fff';
    }
  }

  

  return (
    <>
      {deletionDialog}
      {assignedToDialog}
      <Card elevation={3} style={{ padding: '16px', margin: '10px' }}>
        <CardContent>
          <Grid container spacing={2}>
            <Grid item xs={4.5} md={4.5} sx={{ display: { xs: 'none', sm: 'block' } }}>
              <Typography variant="h6" component="div">
                Lista de reproducción
              </Typography>
            </Grid>

            <Grid item xs={3.5} md={3.5}>
              <Select 
                labelId="select-playlist"
                id="select-playlist"
                value={currentPlaylist}
                onChange={handleChangePlaylist}
                size="small"
                style={{ width: 250 }}
              >
                <MenuItem value='user_playlist'>Mi lista</MenuItem> 
                {originPresetPlaylists.map((originPlaylist) => (
                  <MenuItem key={originPlaylist.name} value={originPlaylist.name}>{originPlaylist.name}</MenuItem>
                ))}
              </Select>
            </Grid>

            <Grid item xs={4} md={4} style={{ textAlign: 'right', marginLeft: 'auto' }} sx={{ display: { xs: 'none', sm: 'block' } }}>
              {currentPlaylist === 'user_playlist' && (
                <>
                  <Tooltip title="Exportar lista">
                    <span>
                      <IconButton style={{ color: '#555' }} onClick={handleExport}>
                        <FileUploadIcon />
                      </IconButton>
                    </span>
                  </Tooltip>

                  <Tooltip title="Importar lista">
                    <span>
                      <IconButton style={{ color: '#555' }} onClick={handleImport}>
                        <GetAppIcon />
                      </IconButton>
                    </span>
                  </Tooltip>
                </>
              )}
            </Grid>
          </Grid>

          <div id="playList" className="playlist_container scroll">
            <DragDropContext onDragEnd={handleDragEndPlaylist}>
              <Droppable droppableId="droppable" direction="vertical">
                {(provided) => (
                  <div ref={provided.innerRef} {...provided.droppableProps}>
                    {playlistObj?.map((item, index) => (
                      <Draggable key={item.id} draggableId={item.id} index={index}>
                        {(provided) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            <Card elevation={3} style={{ margin: '10px 0' }}>
                              <CardContent>
                                <Grid container spacing={2} direction="row" justifyContent="center" alignItems="center">
                                  <Grid item xs={2} md={1}>
                                    <Checkbox checked={item.played} onClick={(e) => handleCheckVideo(e.target.checked, item.id)} />
                                  </Grid>
                                  <Grid item xs={10} md={2}>
                                    <img alt="Preview miniatura del video" src={item.thumbnail} style={{ width: '100%' }} />
                                  </Grid>
                                  <Grid item xs={10} md={7}>
                                    <Typography variant="body1"  component="div" sx={{ textDecoration: item.played ? 'line-through' : 'none', color: getColorItem(item) }}>
                                      {item.title}
                                    </Typography>
                                    {item.assignedTo ? (
                                      <Chip label={item.assignedTo} icon={<AccountCircle />} onDelete={() => handleDeleteAssignedTo(item.id)} color="primary" style={{ marginTop: 10 }} />
                                    ) : (
                                      <Chip variant="outlined" label="No asignado" deleteIcon={<EditIcon />} onDelete={() => handleOpenEditAssignedToDialog(item.id)} style={{ marginTop: 10 }} />
                                    )}
                                  </Grid>
                                  <Grid item xs={2} md={2} style={{ textAlign: 'right' }}>
                                    {currentPlaylist === 'user_playlist' && (
                                      <Tooltip title="Eliminar de la lista de reproducción">
                                        <IconButton onClick={() => handleOpenDeletionDialog(item.id)}>
                                          <ClearIcon />
                                        </IconButton>
                                      </Tooltip>
                                    )}
                                    <Tooltip title="Reproducir ahora" sx={{ display: { xs: 'none', sm: 'block' } }}>
                                      <IconButton onClick={() => playNow(item)}>
                                        <SmartDisplayIcon />
                                      </IconButton>
                                    </Tooltip>
                                  </Grid>
                                </Grid>
                              </CardContent>
                            </Card>
                          </div>
                        )}
                      </Draggable>
                    ))}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </div>
        </CardContent>
      </Card>
    </>
  );
};

export default memo(PlayListCard);
