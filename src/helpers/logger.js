const levels = {
    error: 0,
    warn: 1,
    info: 2,
    debug: 3,
  };
  
  const currentLevel = process.env.REACT_APP_DEBUG_LEVEL || 'info';
    //version 1
    /*
    const getCallerInfo = () => {
      const stack = new Error().stack;
      const stackLines = stack.split('\n');
      // Ajusta el índice basado en cómo se genera la pila de llamadas en tu entorno
      const callerLine = stackLines[3];
      const match = callerLine.match(/at (.+?) \((.+?):(\d+):(\d+)\)/);
      
      if (match) {
        const filePath = match[1];
        const lineNumber = match[3];
        const columnNumber = match[4];
        return `${filePath}:${lineNumber}:${columnNumber}`;
      }
      return 'unknown';
    };
    */
    //version 2
    const getCallerInfo = () => {
      const stack = new Error().stack;
      const stackLines = stack.split('\n');
      const callerLine = stackLines[4];
      const functionNameMatch = callerLine.match(/at (\w+)/);
      const functionName = functionNameMatch ? functionNameMatch[1] : 'unknown file';
      //const lineNumberMatch = stackLines[6].split(':');
      //const lineNumber = lineNumberMatch && lineNumberMatch.length && lineNumberMatch[4] ? lineNumberMatch[4] : 'unknown line';
      //return `${functionName}:${lineNumber}`;
      return `${functionName}`;
  };


  const logger = {
    log: (level, ...args) => {
      if (levels[level] <= levels[currentLevel]) {
        const callerInfo = getCallerInfo();
        console[level](`[${callerInfo}]`, ...args);
      }
    },
    error: (...args) => logger.log('error', ...args),
    warn: (...args) => logger.log('warn', ...args),
    info: (...args) => logger.log('info', ...args),
    debug: (...args) => logger.log('debug', ...args),
  };
  
  export default logger;
  