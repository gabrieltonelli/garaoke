import React, { useEffect, useState, useCallback } from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import SnackMessage from './components/SnackMessage';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { auth, db, doc, setDoc, getDoc, updateDoc } from './components/FirebaseConfig';
import logger from './helpers/logger';
import { Box, CircularProgress } from '@mui/material';
import Monitor from './pages/Monitor';
import MonitorLogin from './pages/MonitorLogin';

// Pages
import Config from './pages/Config';
import Player from './pages/Player';
import Pannel from './pages/Pannel';
import Login from './pages/Login';
import NotFound from './pages/NotFound';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});

const emptyUserSettings = {
  configs: {
    general: {
      showControlsPlayer: true,
      allowInteractivity: true,       
      theme: 'dark',
    },
    apiKeys: Array(5).fill(''),
    intros: Array(5).fill(''),
    transitions: Array(5).fill(''),
  },
  playlist: [],
  presetPlaylists: [],
};

const App = () => {
  const [keySearch, setKeySearch] = useState('');
  const [currentIntroVideo, setCurrentIntroVideo] = useState(process.env.REACT_APP_DEFAULT_INTRO_VIDEO_CODE);
  const [currentTransitionVideo, setCurrentTransitionVideo] = useState(process.env.REACT_APP_DEFAULT_TRANSITION_VIDEO_CODE);
  const [videosSearchResponse, setVideosSearchResponse] = useState([]);
  const [previewVideId, setPreviewVideoId] = useState(null);
  const [isKaraokeOnly, setIsKaraokeOnly] = useState(true);
  const [playlist, setPlaylist] = useState([]);
  const [currentPlaylist, setCurrentPlaylist] = useState('user_playlist');
  const [user, setUser] = useState(-1);
  const [message, setMessage] = useState({ text: '', type: '' });
  const [videoPlayer, setVideoPlayer] = useState(null);
  const [selectedApiKey, setSelectedApiKey] = useState(process.env.REACT_APP_DEFAULT_YOUTUBE_API_KEY);
  const [originPresetPlaylists, setOriginPresetPlaylists] = useState([]);
  const [originPresetPlaylist, setOriginPresetPlaylist] = useState([]);
  const [userPresetPlaylists, setUserPresetPlaylists] = useState([]);
  const [userPresetPlaylist, setUserPresetPlaylist] = useState([]);
  const [userSettings, setUserSettings] = useState(emptyUserSettings);
  const [showModalQR, setShowModalQR] = useState(false);
  const [showModalVotesTable, setShowModalVotesTable] = useState(false);
  const [showModalMessage, setShowModalMessage] = useState(false);
  const [currentPlaylistItem, setCurrentPlaylistItem] = useState(null);
  const [allowInteractivity, setAllowInteractivity] = useState(true);
  const [showControlsPlayer, setShowControlsPlayer] = useState(true);
  const [videoId, setVideoId] = useState(null);
  
  /* eslint-disable */

  const openVideoPlayer = useCallback((videoId = currentTransitionVideo) => {
    if (videoPlayer && !videoPlayer.closed && videoPlayer.name) {
      videoPlayer.postMessage({ type: 'changeVideo', videoId: videoId }, '*');
    } else {
      
      setVideoPlayer(window.open(`/player?videoId=${videoId}&userId=${user.uid}`, 'videoPlayer'));
    }
    setVideoId(videoId);
  }, [videoPlayer, user]);

  const showHideModalQR = useCallback(() => {
    if (videoPlayer && !videoPlayer.closed) {
      videoPlayer.postMessage({ type: 'showHideModalQR', modalQR: showModalQR, query: '?UID='+user.uid }, '*');
      setShowModalQR(!showModalQR);
    }
  }, [videoPlayer]);

  const showHideModalVotesTable = useCallback(() => {
    if (videoPlayer && !videoPlayer.closed) {
      videoPlayer.postMessage({ type: 'showHideModalVotesTable', modalVotes: showModalVotesTable }, '*');
      setShowModalVotesTable(!showModalVotesTable);
    }
  }, [videoPlayer, showModalVotesTable]);
  
  const showHideModalMessage = useCallback((message) => {
    if(showModalMessage === true) {
      setShowModalMessage(false)
    }else{
      setShowModalMessage(true)
    }
    if (videoPlayer && !videoPlayer.closed) {
      setTimeout(()=>videoPlayer.postMessage({ type: 'showHideModalMessage', modalMessage: showModalMessage, message: message }, '*'),1000);
    }
  }, [showModalMessage, videoPlayer]);

  const changeIntro = useCallback((introId) => {
    if (videoPlayer && !videoPlayer.closed) {
      videoPlayer.postMessage({ type: 'changeIntro', introId: introId }, '*');
      setCurrentIntroVideo(introId);
    }
  }, [videoPlayer]);

  const changeTransition = useCallback((transitionId) => {
    if (videoPlayer && !videoPlayer.closed) {
      videoPlayer.postMessage({ type: 'changeTransition', transitionId: transitionId }, '*');
      setCurrentTransitionVideo(transitionId);
    }
  }, [videoPlayer]);

  const changePlaylistItem = useCallback((playlistItem) => {
    if (videoPlayer && !videoPlayer.closed) {
      videoPlayer.postMessage({ type: 'changePlaylistItem', playlistItem: playlistItem }, '*');
      setCurrentPlaylistItem(playlistItem);
    }
  }, [videoPlayer]);

  const handleGoogleSignOut = useCallback(() => {
    auth.signOut();
    fullReset(false);
    setUser(null);
  }, []);

  useEffect(() => {
    fullReset();
    auth.onAuthStateChanged((user) => {
      if (user !== -1 && user) {
        getUserSettings(user.uid).then(() => setUser(user));
      } else {
        fullReset(false);
        setUser(null);
      }
    });
  }, []);

  /* eslint-enable */

  const fullReset = (resetUser = true) => {
    if (resetUser) setUser(-1);
    setKeySearch('');
    setCurrentIntroVideo(process.env.REACT_APP_DEFAULT_INTRO_VIDEO_CODE);
    setCurrentTransitionVideo(process.env.REACT_APP_DEFAULT_TRANSITION_VIDEO_CODE);
    setVideosSearchResponse([]);
    setPreviewVideoId(null);
    setIsKaraokeOnly(true);
    setPlaylist([]);
    setCurrentPlaylist('user_playlist');
    setVideoPlayer(null);
    setSelectedApiKey(process.env.REACT_APP_DEFAULT_YOUTUBE_API_KEY);
    setOriginPresetPlaylists([]);
    setOriginPresetPlaylist([]);
    setUserPresetPlaylists([]);
    setUserPresetPlaylist([]);
    setUserSettings(emptyUserSettings);
    setAllowInteractivity(true)
  };

  /* eslint-disable*/

  const getUserSettings = useCallback(async (userId) => {
    try {
      const userRef = doc(db, 'users', userId);
      fullReset(false);
      const userSnapshot = await getDoc(userRef);
      if (userSnapshot.exists()) {
        const settings = userSnapshot.data();
        setUserSettings({
          configs: {
            general: {
              showControlsPlayer: settings.configs.general.showControlsPlayer ?? true,
              allowInteractivity: settings.configs.general.allowInteractivity ?? true,
              theme: settings.configs.general.theme === 'light' ? 'light' : 'dark',
            },
            apiKeys: settings.configs.apiKeys ?? Array(5).fill(''),
            intros: settings.configs.intros ?? Array(5).fill(''),
            transitions: settings.configs.transitions ?? Array(5).fill(''),
          },
          playlist: settings.playlist ?? [],
          presetPlaylists: settings.presetPlaylists ?? [],
        });
        setPlaylist(settings.playlist ?? []);
        setUserPresetPlaylists(settings.presetPlaylists ?? []);
        setAllowInteractivity(settings.configs.general.allowInteractivity ?? true)
        setShowControlsPlayer(settings.configs.general.showControlsPlayer ?? true)
      } else {
        setUserSettings(emptyUserSettings);
      }
    } catch (error) {
      setMessage({ text: `Error al recuperar la configuración del usuario: ${error}`, type: 'error' });
      return null;
    }
  }, []);

  const saveUserSettings = useCallback(async (userId, settings) => {
    try {
      const userRef = doc(db, 'users', userId);
      await setDoc(userRef, settings);
      setUserSettings(settings);
      setMessage({ text: 'Configuración guardada correctamente.', type: 'success' });
    } catch (error) {
      setMessage({ text: `Error al guardar la configuración: ${JSON.stringify(error)}`, type: 'error' });
    }
  }, []);

  const handleSaveUserPlaylist = useCallback((newPlaylist) => {
    const newUserSettings = { ...userSettings };
    if (currentPlaylist === 'user_playlist') {
      newUserSettings.playlist = newPlaylist;
      setPlaylist(newPlaylist);
    } else {
      newUserSettings.presetPlaylists = generateNewPresetPlaylists(newPlaylist, [...newUserSettings.presetPlaylists]);
      setUserPresetPlaylists(newUserSettings.presetPlaylists);
      setUserPresetPlaylist(newPlaylist);
    }
    auth.onAuthStateChanged((user) => {
      saveUserSettings(user.uid, newUserSettings);
    });
  }, [currentPlaylist, saveUserSettings, userSettings]);

  /* eslint-enable*/

  const generateNewPresetPlaylists = useCallback((newPlaylist, newUserSettingsPresetPlaylists) => {
    const newPlaylistObj = {
      name: currentPlaylist,
      items: newPlaylist,
    };
    const match = newUserSettingsPresetPlaylists.some((playlist, index) => {
      if (playlist.name === currentPlaylist) {
        newUserSettingsPresetPlaylists[index] = { ...newPlaylistObj };
        return true;
      }
      return false;
    });
    if (match) {
      return [...newUserSettingsPresetPlaylists];
    } else {
      return [...newUserSettingsPresetPlaylists, { ...newPlaylistObj }];
    }
  }, [currentPlaylist]);

  const updateUserField = async (field, value, uid = user.uid) => {
    //console.log('user', user)
    if (!user || user === -1) return;
    let obj = {}
    obj[field] = value
    const docRef = doc(db, 'users', uid);
    try {
      await updateDoc(docRef, {
        ...obj
      });
    } catch (error) {
      console.error('Error updating firebase: ', error);
    }
  };

  return (
    <ThemeProvider theme={darkTheme}>
      <SnackMessage message={message} setMessage={setMessage} duration={message.duration} />
      <CssBaseline />
      <Router>
        <Routes>
          <Route
            path="/"
            element={
              user === -1 ? (
                <Box display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
                  <CircularProgress style={{ marginRight: 10 }} />
                  Cargando...
                </Box>
              ) : user !== null ? (
                <Pannel
                  setKeySearch={setKeySearch}
                  keySearch={keySearch}
                  videosSearchResponse={videosSearchResponse}
                  setPlaylist={handleSaveUserPlaylist}
                  setVideosSearchResponse={setVideosSearchResponse}
                  previewVideId={previewVideId}
                  onIsKaraokeOnly={setIsKaraokeOnly}
                  isKaraokeOnly={isKaraokeOnly}
                  playlist={playlist}
                  setPreviewVideoId={setPreviewVideoId}
                  setCurrentIntroVideo={changeIntro}
                  setCurrentTransitionVideo={changeTransition}
                  currentIntroVideo={currentIntroVideo}
                  currentTransitionVideo={currentTransitionVideo}
                  user={user}
                  userSettings={userSettings}
                  openVideoPlayer={openVideoPlayer}
                  showHideModalQR={showHideModalQR}
                  showHideModalMessage={showHideModalMessage}
                  showModalMessage={showModalMessage}
                  videoPlayer={videoPlayer}
                  setSnackMessage={setMessage}
                  selectedApiKey={selectedApiKey}
                  setSelectedApiKey={setSelectedApiKey}
                  logger={logger}
                  currentPlaylist={currentPlaylist}
                  setCurrentPlaylist={setCurrentPlaylist}
                  userPresetPlaylists={userPresetPlaylists}
                  originPresetPlaylists={originPresetPlaylists}
                  setOriginPresetPlaylists={setOriginPresetPlaylists}
                  originPresetPlaylist={originPresetPlaylist}
                  setOriginPresetPlaylist={setOriginPresetPlaylist}
                  userPresetPlaylist={userPresetPlaylist}
                  setUserPresetPlaylist={setUserPresetPlaylist}
                  handleGoogleSignOut={handleGoogleSignOut}
                  allowInteractivity={allowInteractivity}
                  setCurrentPlaylistItem={changePlaylistItem}
                  currentPlaylistItem={currentPlaylistItem}
                  setVideoId={setVideoId}
                  videoId={videoId}
                  showHideModalVotesTable={showHideModalVotesTable}
                />
              ) : (
                <Login setUser={setUser} />
              )
            }
          />
          <Route
            path="/config"
            element={
              userSettings && user ? (
                <Config 
                  auth={auth} 
                  user={user} 
                  userSettings={userSettings} 
                  saveUserSettings={saveUserSettings}
                  setAllowInteractivity={setAllowInteractivity}
                  allowInteractivity={allowInteractivity}
                  setShowControlsPlayer={setShowControlsPlayer}
                  showControlsPlayer={showControlsPlayer}
                  setVideoId={setVideoId}
                  videoId={videoId}
                   />
              ) : (
                <NotFound />
              )
            }
          />
          <Route path="/player" element={<Player 
                                            updateUserField={updateUserField} 
                                            currentPlaylistItem={currentPlaylistItem} 
                                            currentTransitionVideo={currentTransitionVideo}
                                            currentIntroVideo={currentIntroVideo}
                                            allowInteractivity={allowInteractivity}
                                            showControlsPlayer={showControlsPlayer}
                                            setVideoId={setVideoId}
                                            videoId={videoId}
                                          
                                          />} 
          />
          <Route path="/monitor" element={user === -1 ? (
                <Box display="flex" justifyContent="center" alignItems="center" minHeight="100vh">
                  <CircularProgress style={{ marginRight: 10 }} />
                  Cargando...
                </Box>
              ) : user !== null ? <Monitor user={user} updateUserField={updateUserField}/>: <MonitorLogin setUser={setUser}/>} />
        </Routes>
      </Router>
    </ThemeProvider>
  );
};

export default App;
